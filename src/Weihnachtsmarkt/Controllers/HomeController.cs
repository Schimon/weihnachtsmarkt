﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Weihnachtsmarkt.Providers.Interfaces;

namespace Weihnachtsmarkt.Controllers
{
	public class HomeController : Controller
	{
		private readonly IHomeViewModelProvider _homeViewModelProvider;

		public HomeController(IHomeViewModelProvider homeViewModelProvider)
		{
			this._homeViewModelProvider = homeViewModelProvider;
		}

		public async Task<ActionResult> Index()
		{
			var model = await _homeViewModelProvider.Provide();
			return View(model);
		}
	}
}