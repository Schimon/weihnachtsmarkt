﻿using System.Configuration;
using System.Web.Mvc;
using Weihnachtsmarkt.Models;
using Weihnachtsmarkt.Services;

namespace Weihnachtsmarkt.Controllers
{
    public class LoginController : Controller
    {
        private readonly IAuthenticationService _authenticationService;

        public LoginController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public ActionResult Index()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            var passwordCorrect = _authenticationService.IsPasswordCorrect(viewModel.Password);

            if (passwordCorrect)
            {
                _authenticationService.Login();
                return RedirectToAction("Index", "Home");
            }

            return View(new LoginViewModel
            {
                WrongPassword = true
            });

        }


        public ActionResult Logout()
        {
            if (_authenticationService.IsLoggedIn())
            {
                _authenticationService.Logout();
            }

            return RedirectToAction("Index", "Home");

        }
    }
}