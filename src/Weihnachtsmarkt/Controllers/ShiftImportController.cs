﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Weihnachtsmarkt.Import.Services;
using Weihnachtsmarkt.Models;
using Weihnachtsmarkt.Services;

namespace Weihnachtsmarkt.Controllers
{
    public class ShiftImportController : Controller
    {
        private readonly IShiftImportService _shiftImportService;
        private readonly IAuthenticationService _authenticationService;

        public ShiftImportController(IShiftImportService shiftImportService, IAuthenticationService authenticationService)
        {
            _shiftImportService = shiftImportService;
            _authenticationService = authenticationService;
        }
        public ActionResult Index()
        {
            if (!_authenticationService.IsLoggedIn())
            {
                return Unauthorized();
            }

            return View();
        }

        public async Task<ActionResult> ShiftImportResult()
        {
            if (!_authenticationService.IsLoggedIn())
            {
                return Unauthorized();
            }

            var model = new ShiftImportViewModel
            {
                Result = await _shiftImportService.ImportAsync()
            };

            return View(model);

        }

        private ActionResult Unauthorized()
        {
            Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            return Content(HttpStatusCode.Unauthorized + ": You must be logged in to perform this action", "text/plain");
        }
    }
}