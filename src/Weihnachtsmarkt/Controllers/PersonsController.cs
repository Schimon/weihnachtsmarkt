﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Weihnachtsmarkt.Infrastructure.Services.Interfaces;
using Weihnachtsmarkt.Providers.Interfaces;

namespace Weihnachtsmarkt.Controllers
{
    public class PersonsController : Controller
    {
        private readonly IPersonsViewModelProvider _personsViewModelProvider;
        private readonly IShiftService _shiftService;

        public PersonsController(IPersonsViewModelProvider personsViewModelProvider, IShiftService shiftService)
        {
            _personsViewModelProvider = personsViewModelProvider;
            _shiftService = shiftService;
        }

        public async Task<ActionResult> Index()
        {
            var viewModel = await _personsViewModelProvider.Provide();
            return View(viewModel);
        }
    }
}