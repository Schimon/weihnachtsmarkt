﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Weihnachtsmarkt.Export.Services.Interfaces;
using Weihnachtsmarkt.Models;

namespace Weihnachtsmarkt.Controllers
{
    public class ExportController : Controller
    {
        private readonly IShiftExportService _shiftExportService;

        public ExportController(IShiftExportService shiftExportService)
        {
            _shiftExportService = shiftExportService;
        }

        public async Task<ActionResult> Index()
        {
            var result = await _shiftExportService.Export();

            if (!result.Success)
            {
                return View(ErrorMessage(result.ErrorMessage));
            }

            if (string.IsNullOrWhiteSpace(result.PathToPdf) || !System.IO.File.Exists(result.PathToPdf))
            {
                return View(ErrorMessage("Der Export war erfolgreich, allerdings wurde die Datei nicht gespeichert oder sie konnte nicht gefunden werden."));
            }

            return File(result.PathToPdf, "application/pdf");

        }

        private static ExportViewModel ErrorMessage(string message)
        {
            return new ExportViewModel
            {
                ErrorMessage = message
            };
        }
    }
}