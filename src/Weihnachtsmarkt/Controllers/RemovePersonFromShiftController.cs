﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Weihnachtsmarkt.Infrastructure.Repositories.Interfaces;
using Weihnachtsmarkt.Models.Persons;
using Weihnachtsmarkt.Providers;
using Weihnachtsmarkt.Services;

namespace Weihnachtsmarkt.Controllers
{
    public class RemovePersonFromShiftController : Controller
    {
        private readonly IRemovePersonFromShiftViewModelProvider _removePersonFromShiftViewModelProvider;
        private readonly IBookedShiftsRepository _bookedShiftsRepository;
        private readonly IAuthenticationService _authenticationService;

        public RemovePersonFromShiftController(
            IRemovePersonFromShiftViewModelProvider removePersonFromShiftViewModelProvider, 
            IBookedShiftsRepository bookedShiftsRepository,
            IAuthenticationService authenticationService)
        {
            _removePersonFromShiftViewModelProvider = removePersonFromShiftViewModelProvider;
            _bookedShiftsRepository = bookedShiftsRepository;
            _authenticationService = authenticationService;
        }

        public async Task<ActionResult> Index(int shiftId, int workerId)
        {
            if (!_authenticationService.IsLoggedIn())
            {
                return Unauthorized();
            }

            var viewModel = await _removePersonFromShiftViewModelProvider.Provide(shiftId, workerId);
            return View(viewModel);
        }

        public async Task<ActionResult> Confirm(int shiftId, int workerId)
        {
            if (!_authenticationService.IsLoggedIn())
            {
                return Unauthorized();
            }

            try
            {
                await _bookedShiftsRepository.DeleteAsync(shiftId, workerId);
                return Redirect("~/Persons");
            }
            catch(Exception ex)
            {
                return View("Index", new RemovePersonFromShiftViewModel
                {
                    ErrorMessage = ex.Message
                });
            }
        }

        private ActionResult Unauthorized()
        {
            Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            return Content(HttpStatusCode.Unauthorized + ": You must be logged in to perform this action", "text/plain");
        }
    }
}