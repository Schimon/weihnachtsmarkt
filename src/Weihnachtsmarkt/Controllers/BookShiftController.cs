﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Weihnachtsmarkt.Infrastructure.Dtos;
using Weihnachtsmarkt.Infrastructure.Repositories.Interfaces;
using Weihnachtsmarkt.Infrastructure.Services.Interfaces;
using Weihnachtsmarkt.Models;
using Weihnachtsmarkt.Providers.Interfaces;

namespace Weihnachtsmarkt.Controllers
{
    public class BookShiftController : Controller
    {
        private readonly IBookShiftViewModelProvider _bookShiftViewModelProvider;
        private readonly IWorkerRepository _workerRepository;
        private readonly IBookedShiftsRepository _bookedShiftsRepository;
        private readonly IShiftService _shiftService;

        public BookShiftController(
            IBookShiftViewModelProvider bookShiftViewModelProvider, 
            IWorkerRepository workerRepository, 
            IBookedShiftsRepository bookedShiftsRepository,
            IShiftService shiftService)
        {
            _bookShiftViewModelProvider = bookShiftViewModelProvider;
            _workerRepository = workerRepository;
            _bookedShiftsRepository = bookedShiftsRepository;
            _shiftService = shiftService;
        }

        public async Task<ActionResult> Index(int? shiftId)
        {
            if (!shiftId.HasValue || shiftId.Value == 0)
            {
                return View();
            }

            var viewModel = await _bookShiftViewModelProvider.Provide(shiftId.Value);
            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Index(BookShiftViewModel viewModel, int shiftId)
        {
            var resultViewModel = new BookShiftResultViewModel
            {
                ShiftData = await _shiftService.GetShiftWithWorkersAsync(shiftId)
            };

            viewModel.ResultViewModel = resultViewModel;
            viewModel.ShiftData = resultViewModel.ShiftData;

            if (resultViewModel.ShiftData.Workers.Count() >= resultViewModel.ShiftData.NeededWorkers)
            {
                resultViewModel.Message = "Die Schicht ist bereits voll.";
                return View(viewModel);
            }

            if (string.IsNullOrWhiteSpace(viewModel?.FirstName) || string.IsNullOrWhiteSpace(viewModel.LastName))
            {
                resultViewModel.Message = "Bitte geben Sie Ihren vollständigen Vor- und Nachnamen an.";
                return View(viewModel);
            }

            var worker = new WorkerData
            {
                FirstName = viewModel.FirstName,
                LastName = viewModel.LastName
            };

            var workerId = await _workerRepository.InsertIfNotExistsAsync(worker);

            if (await _bookedShiftsRepository.FetchAsync(shiftId, workerId) != null)
            {
                resultViewModel.Message = "Sie sind bereits in dieser Schicht eingetragen";
                return View(viewModel);
            }

            var bookedShift = new BookedShiftData
            {
                Shift = new ShiftData { ShiftId = shiftId },
                Worker = new WorkerData { Id = workerId }
            };

            await _bookedShiftsRepository.InsertAsync(bookedShift);

            resultViewModel.Success = true;

            return View(viewModel);

        }
    }
}