﻿using System.Collections.Generic;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Models.Persons
{
    public class PersonsViewModel
    {
        public IEnumerable<WorkerData> WorkerDatas { get; set; }
        public bool IsCurrentUserLoggedIn { get; set; }
    }
}