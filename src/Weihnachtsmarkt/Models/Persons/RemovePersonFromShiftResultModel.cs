﻿namespace Weihnachtsmarkt.Models.Persons
{
    public class RemovePersonFromShiftResultModel
    {
        public string Success { get; set; }
        public string Message { get; set; }
    }
}