﻿using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Models.Persons
{
    public class RemovePersonFromShiftViewModel
    {
        public ShiftData Shift { get; set; }
        public WorkerData Worker { get; set; }
        public string ErrorMessage { get; set; }
    }
}