﻿namespace Weihnachtsmarkt.Models
{
    public class LoginViewModel
    {
        public string Password { get; set; }
        public bool WrongPassword { get; set; }
    }
}