﻿using System.Collections.Generic;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Models.Home
{
	public class HomeViewModel
	{
	    public IEnumerable<WeekShiftData> Weeks { get; set; }
	}
}
