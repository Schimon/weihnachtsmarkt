﻿using System;
using System.Collections.Generic;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Models.Home
{
    public class WeekShiftModel
    {
        public DateTime WeekStart { get; set; }
        public DateTime WeekEnd { get; set; }
        public IEnumerable<ShiftData> ShiftModels { get; set; }
    }
}