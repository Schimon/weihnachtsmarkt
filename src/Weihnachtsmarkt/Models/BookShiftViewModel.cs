﻿using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Models
{
    public class BookShiftViewModel
    {
        public ShiftData ShiftData { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public BookShiftResultViewModel ResultViewModel { get; set; }
    }
}