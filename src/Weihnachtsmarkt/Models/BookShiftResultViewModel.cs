﻿using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Models
{
    public class BookShiftResultViewModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public ShiftData ShiftData { get; set; }
    }
}