﻿using Weihnachtsmarkt.Import.Dtos;

namespace Weihnachtsmarkt.Models
{
    public class ShiftImportViewModel
    {
        public ShiftImportResult Result { get; set; }
    }
}