﻿using Ninject;

namespace Weihnachtsmarkt.DependenyInjection
{
	public static class KernelLocator
	{
		public static IKernel Kernel { get; set; }
	}
}
