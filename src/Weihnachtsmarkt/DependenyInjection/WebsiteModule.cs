﻿using Ninject.Modules;
using Weihnachtsmarkt.Export.Services;
using Weihnachtsmarkt.Export.Services.Interfaces;
using Weihnachtsmarkt.Import.Parser;
using Weihnachtsmarkt.Import.Services;
using Weihnachtsmarkt.Infrastructure.Mappers;
using Weihnachtsmarkt.Infrastructure.Mappers.Interfaces;
using Weihnachtsmarkt.Infrastructure.Repositories;
using Weihnachtsmarkt.Infrastructure.Repositories.Interfaces;
using Weihnachtsmarkt.Infrastructure.Services;
using Weihnachtsmarkt.Infrastructure.Services.Interfaces;
using Weihnachtsmarkt.Providers;
using Weihnachtsmarkt.Providers.Interfaces;
using Weihnachtsmarkt.Services;

namespace Weihnachtsmarkt.DependenyInjection
{
	public class WebsiteModule : NinjectModule
	{
		public override void Load()
		{
            // Providers
			Bind<IHomeViewModelProvider>().To<HomeViewViewModelProvider>();
		    Bind<IBookShiftViewModelProvider>().To<BookShiftViewModelProvider>();
		    Bind<IPersonsViewModelProvider>().To<PersonsViewModelProvider>();
		    Bind<IRemovePersonFromShiftViewModelProvider>().To<RemovePersonFromShiftViewModelProvider>();

            // ShiftData Import
            Bind<IShiftCsvParser>().To<ShiftCsvParser>();
			Bind<IShiftImportService>().To<ShiftImportService>();

            // Data Repositories
		    Bind<IShiftRepository>().To<ShiftRepository>();
            Bind<IWorkerRepository>().To<WorkerRepository>();
		    Bind<IBookedShiftsRepository>().To<BookedShiftsRepository>();

            // Mappers
            Bind<IShiftDataMapper>().To<ShiftDataMapper>();
		    Bind<IWorkerDataMapper>().To<WorkerDataMapper>();
		    Bind<IBookedShiftstDataMapper>().To<BookedShiftsDataMapper>();

            // Services
		    Bind<IShiftService>().To<ShiftService>();
		    Bind<IWorkerService>().To<WorkerService>();
		    Bind<IAuthenticationService>().To<AuthenticationService>();
		    Bind<IShiftExportService>().To<ShiftExportService>();
		    Bind<IWeekShiftService>().To<WeekShiftService>();
		    Bind<IHtmlToPdfService>().To<HtmlToPdfService>();
        }
    }
}
