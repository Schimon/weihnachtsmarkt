﻿namespace Weihnachtsmarkt.Constatns
{
    public struct SessionKeys
    {
        public const string LoggedIn = "loggedIn";
        public const string LoggedOut = "loggedOut";
    }
}