﻿using Ninject.Web.Common.WebHost;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Ninject;
using Weihnachtsmarkt.DependenyInjection;

namespace Weihnachtsmarkt
{
    public class MvcApplication : NinjectHttpApplication
	{
		protected override IKernel CreateKernel()
		{
			var kernel = new StandardKernel(new WebsiteModule());
			KernelLocator.Kernel = kernel;

			return kernel;
		}

		protected override void OnApplicationStarted()
		{
			base.OnApplicationStarted();

			CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo("de");
			CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo("de");

			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
		}
	}
}
