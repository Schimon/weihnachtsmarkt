﻿namespace Weihnachtsmarkt.Services
{
    public interface IAuthenticationService
    {
        bool IsPasswordCorrect(string password);
        void Login();
        bool IsLoggedIn();
        void Logout();
    }
}