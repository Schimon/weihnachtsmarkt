﻿using System.Configuration;
using System.Web;
using Weihnachtsmarkt.Constatns;

namespace Weihnachtsmarkt.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        public bool IsPasswordCorrect(string password)
        {
            var passwordTemplate = ConfigurationManager.AppSettings["websitePassword"];

            if (string.IsNullOrWhiteSpace(passwordTemplate))
            {
                passwordTemplate = "glühwein";
            }

            return password == passwordTemplate;
        }

        public void Login()
        {
            HttpContext.Current.Session[SessionKeys.LoggedIn] = "true";
        }

        public bool IsLoggedIn()
        {
            return HttpContext.Current.Session[SessionKeys.LoggedIn]?.ToString() == "true";
        }

        public void Logout()
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session[SessionKeys.LoggedOut] = "true";
        }
    }
}