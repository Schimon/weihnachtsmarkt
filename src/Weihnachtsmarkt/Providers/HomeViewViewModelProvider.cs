﻿using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Services.Interfaces;
using Weihnachtsmarkt.Models.Home;
using Weihnachtsmarkt.Providers.Interfaces;

namespace Weihnachtsmarkt.Providers
{
	internal class HomeViewViewModelProvider : IHomeViewModelProvider
	{
	    private readonly IWeekShiftService _weekShiftService;

	    public HomeViewViewModelProvider(IWeekShiftService weekShiftService)
	    {
	        _weekShiftService = weekShiftService;
	    }

	    public async Task<HomeViewModel> Provide()
	    {
	        var weeksList = await _weekShiftService.GetAllShiftsGroupedByWeeks();
            var model = new HomeViewModel
            {
                Weeks = weeksList
            };

	        return model;
	    }
	}
}
