﻿using System.Threading.Tasks;
using Weihnachtsmarkt.Models.Home;

namespace Weihnachtsmarkt.Providers.Interfaces
{
	public interface IHomeViewModelProvider
	{
	    Task<HomeViewModel> Provide();

	}
}
