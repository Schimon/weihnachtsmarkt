﻿using System.Threading.Tasks;
using Weihnachtsmarkt.Models;

namespace Weihnachtsmarkt.Providers.Interfaces
{
    public interface IBookShiftViewModelProvider
    {
        Task<BookShiftViewModel> Provide(int shiftId);
    }
}