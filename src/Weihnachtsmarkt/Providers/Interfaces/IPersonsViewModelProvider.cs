﻿using System.Threading.Tasks;
using Weihnachtsmarkt.Models;
using Weihnachtsmarkt.Models.Persons;

namespace Weihnachtsmarkt.Providers.Interfaces
{
    public interface IPersonsViewModelProvider
    {
        Task<PersonsViewModel> Provide();
    }
}