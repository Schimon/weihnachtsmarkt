﻿using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Services.Interfaces;
using Weihnachtsmarkt.Models;
using Weihnachtsmarkt.Providers.Interfaces;

namespace Weihnachtsmarkt.Providers
{
    internal class BookShiftViewModelProvider : IBookShiftViewModelProvider
    {
        private readonly IShiftService _shiftService;

        public BookShiftViewModelProvider(IShiftService shiftService)
        {
            _shiftService = shiftService;
        }

        public async Task<BookShiftViewModel> Provide(int shiftId)
        {
            var shift = await _shiftService.GetShiftWithWorkersAsync(shiftId);

            if (shift == null)
            {
                return null;
            }
            
            return new BookShiftViewModel
            {
                ShiftData = shift
            };
        }
    }
}