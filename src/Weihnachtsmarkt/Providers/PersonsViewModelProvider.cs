﻿using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Services.Interfaces;
using Weihnachtsmarkt.Models.Persons;
using Weihnachtsmarkt.Providers.Interfaces;
using Weihnachtsmarkt.Services;

namespace Weihnachtsmarkt.Providers
{
    internal class PersonsViewModelProvider : IPersonsViewModelProvider
    {
        private readonly IWorkerService _workerService;
        private readonly IAuthenticationService _authenticationService;

        public PersonsViewModelProvider(IWorkerService workerService, IAuthenticationService authenticationService)
        {
            _workerService = workerService;
            _authenticationService = authenticationService;
        }

        public async Task<PersonsViewModel> Provide()
        {
            var workers = await _workerService.GetAllWorkersWithShifts();
            return new PersonsViewModel
            {
                WorkerDatas = workers,
                IsCurrentUserLoggedIn = _authenticationService.IsLoggedIn()
            };
        }
    }
}