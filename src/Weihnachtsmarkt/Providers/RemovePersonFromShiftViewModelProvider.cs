﻿using System.Linq;
using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Services.Interfaces;
using Weihnachtsmarkt.Models.Persons;

namespace Weihnachtsmarkt.Providers
{
    internal class RemovePersonFromShiftViewModelProvider : IRemovePersonFromShiftViewModelProvider
    {
        private readonly IShiftService _shiftService;

        public RemovePersonFromShiftViewModelProvider(IShiftService shiftService)
        {
            _shiftService = shiftService;
        }

        public async Task<RemovePersonFromShiftViewModel> Provide(int shiftId, int workerId)
        {
            var shift = await _shiftService.GetShiftWithWorkersAsync(shiftId);
            var worker = shift?.Workers.FirstOrDefault(x => x.Id == workerId);

            return new RemovePersonFromShiftViewModel
            {
                Shift = shift,
                Worker = worker
            };
        }
    }
}