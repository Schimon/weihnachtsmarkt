﻿using System.Threading.Tasks;
using Weihnachtsmarkt.Models.Persons;

namespace Weihnachtsmarkt.Providers
{
    public interface IRemovePersonFromShiftViewModelProvider
    {
        Task<RemovePersonFromShiftViewModel> Provide(int shiftId, int workerId);
    }
}