//------------------------------------------------------------------------------
// <auto-generated>
//     Der Code wurde von einer Vorlage generiert.
//
//     Manuelle Änderungen an dieser Datei führen möglicherweise zu unerwartetem Verhalten der Anwendung.
//     Manuelle Änderungen an dieser Datei werden überschrieben, wenn der Code neu generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Weihnachtsmarkt.DatabaseGeneric
{
    using System;
    using System.Collections.Generic;
    
    public partial class Shift
    {
        public int Id { get; set; }
        public int ShiftNumber { get; set; }
        public System.DateTime ShiftStart { get; set; }
        public System.DateTime ShiftEnd { get; set; }
        public int Workers { get; set; }
        public string Description { get; set; }
    }
}
