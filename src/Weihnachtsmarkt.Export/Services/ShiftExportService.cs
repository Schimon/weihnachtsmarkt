﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Weihnachtsmarkt.Export.Dtos;
using Weihnachtsmarkt.Export.Services.Interfaces;
using Weihnachtsmarkt.Infrastructure.Dtos;
using Weihnachtsmarkt.Infrastructure.Services.Interfaces;

namespace Weihnachtsmarkt.Export.Services
{
    public class ShiftExportService : IShiftExportService
    {
        private const string PdfTemplate = @"App_Data\ShiftExport\pdf_template.html";
        private const string TableTemplate = @"App_Data\ShiftExport\table_template.html";
        private const string PdfOutput = @"App_Data\ShiftExport\output\";

        private readonly IWeekShiftService _weekShiftService;
        private readonly IHtmlToPdfService _htmlToPdfService;

        public ShiftExportService(IWeekShiftService weekShiftService, IHtmlToPdfService htmlToPdfService)
        {
            _weekShiftService = weekShiftService;
            _htmlToPdfService = htmlToPdfService;
        }

        public async Task<ExportResult> Export()
        {
            var weeks = await _weekShiftService.GetAllShiftsGroupedByWeeks();
            var currentDirectory = AppDomain.CurrentDomain.BaseDirectory;

            if (string.IsNullOrWhiteSpace(currentDirectory))
            {
                return ErrorResult("Current directory could not be found.");
            }

            var pathToPdfTemplate = currentDirectory + PdfTemplate;
            var pathToTableTemplate = currentDirectory + TableTemplate;

            if (string.IsNullOrWhiteSpace(pathToPdfTemplate) || 
                string.IsNullOrWhiteSpace(pathToTableTemplate) || 
                !File.Exists(pathToPdfTemplate) || 
                !File.Exists(pathToTableTemplate))
            {
                return ErrorResult("Not all template files could be found.");
            }

            var pdfTemplate = File.ReadAllText(pathToPdfTemplate);
            var tableTemplate = File.ReadAllText(pathToTableTemplate);

            if (string.IsNullOrWhiteSpace(pdfTemplate) || string.IsNullOrWhiteSpace(tableTemplate))
            {
                return ErrorResult("Not all templates are valid.");
            }

            var pdfOutputFilePath = currentDirectory + PdfOutput;
            var pdfFileNameWithPath = pdfOutputFilePath + $"Schichten_Export_{DateTime.Now:yyyy-MM-dd_HH-mm}.pdf";

            Directory.CreateDirectory(pdfOutputFilePath);

            var tablesHtmlString = weeks.Aggregate("", (current, week) => current + GetTableHtmlString(tableTemplate, week));

            pdfTemplate = pdfTemplate.Replace("{tables}", tablesHtmlString);
            _htmlToPdfService.SaveAsPdf(pdfTemplate, pdfFileNameWithPath);

            return SuccessResult(pdfFileNameWithPath);
        }

        private static string GetTableHtmlString(string template, WeekShiftData week)
        {
            var htmlString = template.Replace("{TableHeaderRows}", "<td>Nr.</td>" +
                                                  "<td>Tätigkeit</td>" +
                                                  "<td>Tag</td>" +
                                                  "<td>Start</td>" +
                                                  "<td>Ende</td>" +
                                                  "<td>Benötigt</td>" +
                                                  "<td>Personal</td>");

            var tableBodyHtmlString = "";

            foreach (var shift in week.ShiftModels)
            {
                tableBodyHtmlString += "<tr>" +
                                       $"<td>{shift.ShiftNumber}</td>" +
                                       $"<td>{shift.ShiftDescription}</td>" +
                                       $"<td>{shift.ShiftStart:dddd}, {shift.ShiftStart:dd.MM}</td>" +
                                       $"<td>{shift.ShiftStart:HH:mm} Uhr</td>" +
                                       $"<td>{shift.ShiftEnd:HH:mm} Uhr</td>" +
                                       $"<td>{shift.NeededWorkers}</td>" +
                                       $"<td><div class=\"centered-table-cell\">{GetWorkersString(shift.Workers)}</div></td>" +
                                       $"</tr>";
            }

            htmlString = htmlString.Replace("{TableBodyRows}", tableBodyHtmlString);
            htmlString = htmlString.Replace("{TableHeader}", $"{week.WeekStart:dd.MM} - {week.WeekEnd:dd.MM.yyyy}");
            return htmlString;
        }

        private static string GetWorkersString(IEnumerable<WorkerData> workers)
        {
            return workers.Aggregate("", (current, worker) => current + $"<div class=\"worker\">{worker.FirstName} {worker.LastName}</div>");
        }

        private static ExportResult ErrorResult(string message)
        {
            return new ExportResult
            {
                ErrorMessage = message
            };
        }

        private static ExportResult SuccessResult(string pathToPdf)
        {
            return new ExportResult
            {
                Success = true,
                PathToPdf = pathToPdf
            };
        }
    }
}