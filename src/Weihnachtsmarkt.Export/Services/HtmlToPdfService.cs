﻿using SelectPdf;
using Weihnachtsmarkt.Export.Services.Interfaces;

namespace Weihnachtsmarkt.Export.Services
{
    public class HtmlToPdfService : IHtmlToPdfService
    {
        public void SaveAsPdf(string html, string outputPath)
        {
			var renderer = new HtmlToPdf
			{
				Options =
				{
					MarginTop = 20,
					MarginBottom = 20,
					MarginLeft = 10,
					MarginRight = 10
				}
			};

			var pdf = renderer.ConvertHtmlString(html);
			pdf.Save(outputPath);
        }
    }
}