﻿using System.Threading.Tasks;
using Weihnachtsmarkt.Export.Dtos;

namespace Weihnachtsmarkt.Export.Services.Interfaces
{
    public interface IShiftExportService
    {
        Task<ExportResult> Export();
    }
}