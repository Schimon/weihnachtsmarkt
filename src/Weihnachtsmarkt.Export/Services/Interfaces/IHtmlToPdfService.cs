﻿namespace Weihnachtsmarkt.Export.Services.Interfaces
{
    public interface IHtmlToPdfService
    {
        void SaveAsPdf(string html, string outputPath);
    }
}