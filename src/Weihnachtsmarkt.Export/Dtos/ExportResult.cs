﻿namespace Weihnachtsmarkt.Export.Dtos
{
    public class ExportResult
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public string PathToPdf { get; set; }
    }
}