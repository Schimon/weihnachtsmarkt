﻿using System;
using System.Collections.Generic;
using Weihnachtsmarkt.Import.Dtos;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Import.Parser
{
	public class ShiftCsvParser : IShiftCsvParser
	{
		public ParseResult Parse(string[] inputLines)
		{
			var result = new ParseResult
			{
				Success = true,
				ShiftList = new List<ShiftData>()
			};

			var line = 0;
		    var shiftNumber = 1;

			try
			{
				foreach (var textLine in inputLines)
				{
				    line++;
                    if (string.IsNullOrWhiteSpace(textLine) || textLine.StartsWith("#"))
				    {
                        continue;
				    }

					var dataParts = textLine.Split(';');
					var shift = new ShiftData
					{
						ShiftNumber = shiftNumber,
                        ShiftDescription = dataParts[0],
						ShiftStart = DateTime.ParseExact(dataParts[1], "dd.MM.yyyy HH:mm", null),
						ShiftEnd = DateTime.ParseExact(dataParts[2], "dd.MM.yyyy HH:mm", null),
						NeededWorkers = int.Parse(dataParts[3])
					};

					result.ShiftList.Add(shift);
				    shiftNumber++;
				}
			} catch (Exception ex)
			{
				result.Success = false;
				result.ErrorMessage = $"Parse error in line {line}. Please check the syntax<br/><br/>The original error was: {ex.Message}";
			}

			return result;
		}
	}
}
