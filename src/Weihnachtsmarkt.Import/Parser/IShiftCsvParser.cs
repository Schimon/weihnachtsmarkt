﻿using Weihnachtsmarkt.Import.Dtos;

namespace Weihnachtsmarkt.Import.Parser
{
    public interface IShiftCsvParser
	{
		ParseResult Parse(string[] inputLines);
    }
}
