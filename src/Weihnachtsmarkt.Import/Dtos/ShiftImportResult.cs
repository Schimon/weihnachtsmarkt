﻿namespace Weihnachtsmarkt.Import.Dtos
{
	public class ShiftImportResult
	{
		public bool Success { get; set; }
		public string Message { get; set; }
	}
}
