﻿using System.Collections.Generic;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Import.Dtos
{
	public class ParseResult
	{
		public bool Success { get; set; }
		public string ErrorMessage { get; set; }
		public IList<ShiftData> ShiftList { get; set; }
	}
}
