﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Web.Hosting;
using Weihnachtsmarkt.Import.Constants;
using Weihnachtsmarkt.Import.Dtos;
using Weihnachtsmarkt.Import.Parser;
using Weihnachtsmarkt.Infrastructure.Repositories.Interfaces;

namespace Weihnachtsmarkt.Import.Services
{
	public class ShiftImportService : IShiftImportService
	{
		private readonly IShiftCsvParser _shiftCsvParser;
		private readonly IShiftRepository _shiftRepository;
        
		public ShiftImportService(IShiftCsvParser shiftCsvParser, IShiftRepository shiftRepository)
		{
			_shiftCsvParser = shiftCsvParser;
			_shiftRepository = shiftRepository;
		}

		public async Task<ShiftImportResult> ImportAsync()
		{
		    var filePath = HostingEnvironment.ApplicationPhysicalPath + FilePaths.ShiftImportCsvFile;

            if (!File.Exists(filePath))
			{
				return ErrorResult($"File at \"{filePath}\" does not exist");
			}

			var inputLines = File.ReadAllLines(filePath);
			var parseResult = _shiftCsvParser.Parse(inputLines);

			if(!parseResult.Success)
			{
				return ErrorResult($"Error parsing the CSV File:<br/>{parseResult.ErrorMessage}");
			}

			var shiftList = parseResult.ShiftList;

			try
			{
				await _shiftRepository.InsertAsync(shiftList);
                // File.Move(filePath, filePath + ".imported");

				return SuccessResult("Successfully inserted the data into the database");
			}
			catch(Exception ex)
			{
				return ErrorResult("Error inserting the data into the database:<br/>" + ex.Message);
			}
        }

		private static ShiftImportResult ErrorResult(string message)
		{
			return new ShiftImportResult
			{
				Success = false,
				Message = message
			};
        }

		private static ShiftImportResult SuccessResult(string message)
		{
			return new ShiftImportResult
			{
				Success = true,
				Message = message
			};
		}
	}
}
