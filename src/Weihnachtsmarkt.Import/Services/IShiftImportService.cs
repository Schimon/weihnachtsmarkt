﻿using System.Threading.Tasks;
using Weihnachtsmarkt.Import.Dtos;

namespace Weihnachtsmarkt.Import.Services
{
	public interface IShiftImportService
	{
		Task<ShiftImportResult> ImportAsync();
    }
}
