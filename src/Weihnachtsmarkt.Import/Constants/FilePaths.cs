﻿namespace Weihnachtsmarkt.Import.Constants
{
    internal struct FilePaths
    {
        internal const string ShiftImportCsvFile = @"App_Data\ShiftImport\shift_import.csv";
    }
}