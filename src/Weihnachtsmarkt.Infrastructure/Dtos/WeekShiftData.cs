﻿using System;
using System.Collections.Generic;

namespace Weihnachtsmarkt.Infrastructure.Dtos
{
    public class WeekShiftData
    {
        public DateTime WeekStart { get; set; }
        public DateTime WeekEnd { get; set; }
        public IEnumerable<ShiftData> ShiftModels { get; set; }
    }
}