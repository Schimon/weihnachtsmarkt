﻿using System.Collections.Generic;

namespace Weihnachtsmarkt.Infrastructure.Dtos
{
    public class WorkerData
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<ShiftData> Shifts { get; set; }
    }
}