﻿namespace Weihnachtsmarkt.Infrastructure.Dtos
{
    public class BookedShiftData
    {
        public int Id { get; set; }
        public WorkerData Worker { get; set; }
        public ShiftData Shift { get; set; }
    }
}