﻿using System;
using System.Collections.Generic;

namespace Weihnachtsmarkt.Infrastructure.Dtos
{
	public class ShiftData
	{
		public int ShiftId { get; set; }
		public int ShiftNumber { get; set; }
		public DateTime ShiftStart { get; set; }
		public DateTime ShiftEnd { get; set; }
		public int NeededWorkers { get; set; }
        public IEnumerable<WorkerData> Workers { get; set; }
        public string ShiftDescription { get; set; }
	}
}
