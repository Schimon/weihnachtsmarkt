﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Weihnachtsmarkt.DatabaseGeneric;
using Weihnachtsmarkt.Infrastructure.Dtos;
using Weihnachtsmarkt.Infrastructure.Mappers.Interfaces;
using Weihnachtsmarkt.Infrastructure.Repositories.Interfaces;

namespace Weihnachtsmarkt.Infrastructure.Repositories
{
    public class WorkerRepository : IWorkerRepository
    {
        private readonly IWorkerDataMapper _workerDataMapper;

        public WorkerRepository(IWorkerDataMapper workerDataMapper)
        {
            _workerDataMapper = workerDataMapper;
        }

        public async Task InsertAsync(WorkerData worker)
        {
            using (var context = new WeihnachtsmarktEntities())
            {
                context.Workers.Add(_workerDataMapper.MapToEntity(worker));
                await context.SaveChangesAsync();
            }
        }

        public async Task InsertAsync(IEnumerable<WorkerData> workerList)
        {
            using (var context = new WeihnachtsmarktEntities())
            {
                context.Workers.AddRange(workerList.Select(_workerDataMapper.MapToEntity));
                await context.SaveChangesAsync();
            }
        }

        public async Task<int> InsertIfNotExistsAsync(WorkerData worker)
        {
            using (var context = new WeihnachtsmarktEntities())
            {
                var entity = await context.Workers.FirstOrDefaultAsync(x => x.FirstName == worker.FirstName && x.LastName == worker.LastName);

                if (entity != null)
                {
                    return entity.Id;
                }

                entity = context.Workers.Add(_workerDataMapper.MapToEntity(worker));
                await context.SaveChangesAsync();

                return entity.Id;
            }
        }

        public async Task<WorkerData> FetchAsync(int workerId)
        {
            using (var context = new WeihnachtsmarktEntities())
            {
                var entity = await context.Workers.FirstOrDefaultAsync(x => x.Id == workerId);
                return _workerDataMapper.MapToDto(entity);
            }
        }

        public async Task<IEnumerable<WorkerData>> FetchAsync()
        {
            using (var context = new WeihnachtsmarktEntities())
            {
                var entities = await context.Workers.ToListAsync();
                return entities.Select(_workerDataMapper.MapToDto);
            }
        }

        public async Task<IEnumerable<WorkerData>> FetchAllWorkerFromShiftAsync(int shiftId)
        {
            using (var context = new WeihnachtsmarktEntities())
            {
                var workerIds = await context.BookedShifts
                    .Where(x => x.ShiftId == shiftId)
                    .Select(x => x.WorkerId)
                    .ToListAsync();

                var workers = await context.Workers.Where(x => workerIds.Contains(x.Id)).ToListAsync();
                return workers.Select(_workerDataMapper.MapToDto);
            }
        }

        public async Task DeleteAsync(int workerId)
        {
            using (var context = new WeihnachtsmarktEntities())
            {
                var sqlStatement = $"DELETE FROM {nameof(context.Workers)} WHERE ID={workerId}";
                await context.Database.ExecuteSqlCommandAsync(sqlStatement);
            }
        }

        public async Task<WorkerData> FetchAsync(string firstName, string lastName)
        {
            using (var context = new WeihnachtsmarktEntities())
            {
                var entity = await context.Workers.FirstOrDefaultAsync(x => x.FirstName == firstName && x.LastName == lastName);

                if (entity == null)
                {
                    return null;
                }

                return _workerDataMapper.MapToDto(entity);
            }
        }
    }
}