﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Weihnachtsmarkt.DatabaseGeneric;
using Weihnachtsmarkt.Infrastructure.Dtos;
using Weihnachtsmarkt.Infrastructure.Mappers.Interfaces;
using Weihnachtsmarkt.Infrastructure.Repositories.Interfaces;

namespace Weihnachtsmarkt.Infrastructure.Repositories
{
	public class ShiftRepository : IShiftRepository
	{
	    private readonly IShiftDataMapper _shiftDataMapper;

	    public ShiftRepository(IShiftDataMapper shiftDataMapper)
	    {
	        _shiftDataMapper = shiftDataMapper;
	    }

	    public async Task InsertAsync(ShiftData shift)
	    {
	        using(var context = new WeihnachtsmarktEntities())
	        {
	            context.Shifts.Add(_shiftDataMapper.MapToEntity(shift));
	            await context.SaveChangesAsync();
	        }
	    }

	    public async Task InsertAsync(IEnumerable<ShiftData> shiftList)
	    {
	        using (var context = new WeihnachtsmarktEntities())
	        {
	            context.Shifts.AddRange(shiftList.Select(_shiftDataMapper.MapToEntity));
	            await context.SaveChangesAsync();
	        }
	    }

	    public async Task<ShiftData> FetchAsync(int shiftId)
	    {
	        using (var context = new WeihnachtsmarktEntities())
	        {
	            var entity = await context.Shifts.FirstOrDefaultAsync(x => x.Id == shiftId);
	            return _shiftDataMapper.MapToDto(entity);
	        }
	    }

	    public async Task<IEnumerable<ShiftData>> FetchAsync()
	    {
	        using (var context = new WeihnachtsmarktEntities())
	        {
	            var entities = await context.Shifts.ToListAsync();
	            return entities.Select(_shiftDataMapper.MapToDto);
	        }
	    }

	    public async Task<IEnumerable<ShiftData>> FetchAllShiftsFromWorkerAsync(int workerId)
	    {
	        using (var context = new WeihnachtsmarktEntities())
	        {
	            var shiftIds = await context.BookedShifts
	                .Where(x => x.WorkerId == workerId)
	                .Select(x => x.ShiftId)
	                .ToListAsync();

	            var shifts = await context.Shifts.Where(x => shiftIds.Contains(x.Id)).ToListAsync();
	            return shifts.Select(_shiftDataMapper.MapToDto);
	        }
	    }

        public async Task DeleteAsync(int shiftId)
	    {
	        using (var context = new WeihnachtsmarktEntities())
	        {
	            var sqlStatement = $"DELETE FROM {nameof(context.Shifts)} WHERE ID={shiftId}";
	            await context.Database.ExecuteSqlCommandAsync(sqlStatement);
	        }
	    }
	}
}
