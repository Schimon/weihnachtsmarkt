﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Infrastructure.Repositories.Interfaces
{
    public interface IShiftRepository
    {
        Task InsertAsync(ShiftData shift);
		Task InsertAsync(IEnumerable<ShiftData> shiftList);
	    Task<ShiftData> FetchAsync(int shiftId);
		Task<IEnumerable<ShiftData>> FetchAsync();
        Task<IEnumerable<ShiftData>> FetchAllShiftsFromWorkerAsync(int workerId);
        Task DeleteAsync(int shiftId);
    }
}