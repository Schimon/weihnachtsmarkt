﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Infrastructure.Repositories.Interfaces
{
    public interface IWorkerRepository
    {
        Task InsertAsync(WorkerData worker);
        Task InsertAsync(IEnumerable<WorkerData> workerList);
        Task<int> InsertIfNotExistsAsync(WorkerData worker);
        Task<WorkerData> FetchAsync(int workerId);
        Task<IEnumerable<WorkerData>> FetchAsync();
        Task<WorkerData> FetchAsync(string firstName, string lastName);
        Task<IEnumerable<WorkerData>> FetchAllWorkerFromShiftAsync(int shiftId);
        Task DeleteAsync(int workerId);
    }
}