﻿using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Infrastructure.Repositories.Interfaces
{
    public interface IBookedShiftsRepository
    {
        Task InsertAsync(BookedShiftData bookedShift);
        Task<BookedShiftData> FetchAsync(int shiftId, int workerId);
        Task DeleteAsync(int shiftId, int workerId);
    }
}