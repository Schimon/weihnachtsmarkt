﻿using System.Data.Entity;
using System.Threading.Tasks;
using Weihnachtsmarkt.DatabaseGeneric;
using Weihnachtsmarkt.Infrastructure.Dtos;
using Weihnachtsmarkt.Infrastructure.Mappers.Interfaces;
using Weihnachtsmarkt.Infrastructure.Repositories.Interfaces;

namespace Weihnachtsmarkt.Infrastructure.Repositories
{
    public class BookedShiftsRepository : IBookedShiftsRepository
    {
        private readonly IBookedShiftstDataMapper _bookedShiftstDataMapper;

        public BookedShiftsRepository(IBookedShiftstDataMapper bookedShiftstDataMapper)
        {
            _bookedShiftstDataMapper = bookedShiftstDataMapper;
        }

        public async Task InsertAsync(BookedShiftData bookedShift)
        {
            using (var context = new WeihnachtsmarktEntities())
            {
                context.BookedShifts.Add(_bookedShiftstDataMapper.MapToEntity(bookedShift));
                await context.SaveChangesAsync();
            }
        }

        public async Task<BookedShiftData> FetchAsync(int shiftId, int workerId)
        {
            using (var context = new WeihnachtsmarktEntities())
            {
                var entity = await context.BookedShifts.FirstOrDefaultAsync(x => x.ShiftId == shiftId && x.WorkerId == workerId);

                if (entity == null)
                {
                    return null;
                }

                return _bookedShiftstDataMapper.MapToDto(entity);
            }
        }

        public async Task DeleteAsync(int shiftId, int workerId)
        {
            using (var context = new WeihnachtsmarktEntities())
            {
                var sqlStatement = $"DELETE FROM {nameof(context.BookedShifts)} WHERE shiftId={shiftId} AND workerId={workerId}";
                await context.Database.ExecuteSqlCommandAsync(sqlStatement);
            }
        }
    }
}