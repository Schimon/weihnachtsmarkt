﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Infrastructure.Services.Interfaces
{
    public interface IWeekShiftService
    {
        Task<IEnumerable<WeekShiftData>> GetAllShiftsGroupedByWeeks();
    }
}