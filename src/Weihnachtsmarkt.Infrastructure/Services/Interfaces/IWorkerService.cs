﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Infrastructure.Services.Interfaces
{
    public interface IWorkerService
    {
        Task<WorkerData> GetWorkerAsync(int workerId);
        Task<IEnumerable<WorkerData>> GetAllWorkersAsync();
        Task<WorkerData> GetWorkerWithShifts(int workerId);
        Task<IEnumerable<WorkerData>> GetAllWorkersWithShifts();
    }
}