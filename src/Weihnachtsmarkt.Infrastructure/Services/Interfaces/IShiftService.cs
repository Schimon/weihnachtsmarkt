﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Infrastructure.Services.Interfaces
{
    public interface IShiftService
    {
        Task<ShiftData> GetShiftAsync(int shiftId);
        Task<ShiftData> GetShiftWithWorkersAsync(int shitftId);
        Task<IEnumerable<ShiftData>> GetAllShiftsAsync();
        Task<IEnumerable<ShiftData>> GetAllShiftsWithWorkersAsync();
    }
}