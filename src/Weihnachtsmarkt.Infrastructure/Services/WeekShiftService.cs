﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.DateTimeExtensions;
using Weihnachtsmarkt.Infrastructure.Dtos;
using Weihnachtsmarkt.Infrastructure.Services.Interfaces;

namespace Weihnachtsmarkt.Infrastructure.Services
{
    public class WeekShiftService : IWeekShiftService
    {
        private readonly IShiftService _shiftService;

        public WeekShiftService(IShiftService shiftService)
        {
            _shiftService = shiftService;
        }

        public async Task<IEnumerable<WeekShiftData>> GetAllShiftsGroupedByWeeks()
        {
            var allShifts = await _shiftService.GetAllShiftsWithWorkersAsync();
            var weeks = allShifts
                .GroupBy(x => CultureInfo.CurrentCulture.DateTimeFormat.Calendar.GetWeekOfYear(x.ShiftStart, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday))
                .ToList();

            var weeksList = weeks.Select(shiftWeekList => MapToWeekShiftModel(shiftWeekList.ToList()));
            return weeksList;
        }

        private static WeekShiftData MapToWeekShiftModel(IList<ShiftData> shiftList)
        {
            var weekShiftModel = new WeekShiftData
            {
                WeekStart = shiftList[0].ShiftStart.FirstDayOfWeek(),
                WeekEnd = shiftList[0].ShiftStart.LastDayOfWeek(),
                ShiftModels = shiftList
            };

            return weekShiftModel;
        }
    }
}