﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Dtos;
using Weihnachtsmarkt.Infrastructure.Repositories.Interfaces;
using Weihnachtsmarkt.Infrastructure.Services.Interfaces;

namespace Weihnachtsmarkt.Infrastructure.Services
{
    public class WorkerService : IWorkerService
    {
        private readonly IWorkerRepository _workerRepository;
        private readonly IShiftRepository _shiftRepository;

        public WorkerService(IWorkerRepository workerRepository, IShiftRepository shiftRepository)
        {
            _workerRepository = workerRepository;
            _shiftRepository = shiftRepository;
        }

        public async Task<WorkerData> GetWorkerAsync(int workerId)
        {
            var worker = await _workerRepository.FetchAsync(workerId);
            return worker;
        }

        public async Task<IEnumerable<WorkerData>> GetAllWorkersAsync()
        {
            var workers = await _workerRepository.FetchAsync();
            return workers;
        }

        public async Task<WorkerData> GetWorkerWithShifts(int workerId)
        {
            var worker = await _workerRepository.FetchAsync(workerId);
            var shifts = await _shiftRepository.FetchAllShiftsFromWorkerAsync(workerId);

            if (shifts == null)
            {
                shifts = new List<ShiftData>();
            }

            worker.Shifts = shifts;
            return worker;
        }

        public async Task<IEnumerable<WorkerData>> GetAllWorkersWithShifts()
        {
            var workers = (await _workerRepository.FetchAsync()).ToList();

            foreach (var worker in workers)
            {
                var shifts = await _shiftRepository.FetchAllShiftsFromWorkerAsync(worker.Id);

                if (shifts == null)
                {
                    shifts = new List<ShiftData>();
                }

                worker.Shifts = shifts;
            }

            return workers;
        }

    }
}