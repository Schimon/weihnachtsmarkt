﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Weihnachtsmarkt.Infrastructure.Dtos;
using Weihnachtsmarkt.Infrastructure.Repositories.Interfaces;
using Weihnachtsmarkt.Infrastructure.Services.Interfaces;

namespace Weihnachtsmarkt.Infrastructure.Services
{
    public class ShiftService : IShiftService
    {
        private readonly IShiftRepository _shiftRepository;
        private readonly IWorkerRepository _workerRepository;

        public ShiftService(IShiftRepository shiftRepository, IWorkerRepository workerRepository)
        {
            _shiftRepository = shiftRepository;
            _workerRepository = workerRepository;
        }

        public async Task<ShiftData> GetShiftAsync(int shiftId)
        {
            var shift = await _shiftRepository.FetchAsync(shiftId);
            return shift;
        }

        public async Task<IEnumerable<ShiftData>> GetAllShiftsAsync()
        {
            var shifts = await _shiftRepository.FetchAsync();
            return shifts;
        }

        public async Task<ShiftData> GetShiftWithWorkersAsync(int shitftId)
        {
            var shift = await _shiftRepository.FetchAsync(shitftId);
            if (shift == null)
            {
                return null;
            }

            var workers = await _workerRepository.FetchAllWorkerFromShiftAsync(shitftId);

            if (workers == null)
            {
                workers = new List<WorkerData>();
            }

            shift.Workers = workers;
            return shift;
        }

        public async Task<IEnumerable<ShiftData>> GetAllShiftsWithWorkersAsync()
        {
            var shifts = (await _shiftRepository.FetchAsync()).ToList();

            foreach (var shift in shifts)
            {
                var workers = await _workerRepository.FetchAllWorkerFromShiftAsync(shift.ShiftId);

                if (workers == null)
                {
                    workers = new List<WorkerData>();
                }

                shift.Workers = workers;
            }

            return shifts;
        }
    }
}