﻿using Weihnachtsmarkt.DatabaseGeneric;
using Weihnachtsmarkt.Infrastructure.Dtos;
using Weihnachtsmarkt.Infrastructure.Mappers.Interfaces;

namespace Weihnachtsmarkt.Infrastructure.Mappers
{
    public class ShiftDataMapper : IShiftDataMapper
    {
        public ShiftData MapToDto(Shift entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new ShiftData
            {
                ShiftId = entity.Id,
                ShiftNumber = entity.ShiftNumber,
                ShiftStart = entity.ShiftStart,
                ShiftEnd = entity.ShiftEnd,
                NeededWorkers = entity.Workers,
                ShiftDescription = entity.Description
            };
        }

        public Shift MapToEntity(ShiftData shift)
        {
            if (shift == null)
            {
                return null;
            }

            return new Shift
            {
                Id = shift.ShiftId,
                ShiftNumber = shift.ShiftNumber,
                ShiftStart = shift.ShiftStart,
                ShiftEnd = shift.ShiftEnd,
                Workers = shift.NeededWorkers,
                Description = shift.ShiftDescription
            };
        }
    }
}