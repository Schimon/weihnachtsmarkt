﻿using Weihnachtsmarkt.DatabaseGeneric;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Infrastructure.Mappers.Interfaces
{
    public interface IWorkerDataMapper
    {
        WorkerData MapToDto(Worker entity);
        Worker MapToEntity(WorkerData shift);
    }
}