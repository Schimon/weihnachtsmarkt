﻿using Weihnachtsmarkt.DatabaseGeneric;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Infrastructure.Mappers.Interfaces
{
    public interface IShiftDataMapper
    {
        ShiftData MapToDto(Shift entity);
        Shift MapToEntity(ShiftData shift);
    }
}