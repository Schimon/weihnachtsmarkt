﻿using Weihnachtsmarkt.DatabaseGeneric;
using Weihnachtsmarkt.Infrastructure.Dtos;

namespace Weihnachtsmarkt.Infrastructure.Mappers.Interfaces
{
    public interface IBookedShiftstDataMapper
    {
        BookedShiftData MapToDto(BookedShift entity);
        BookedShift MapToEntity(BookedShiftData bookedShift);
    }
}