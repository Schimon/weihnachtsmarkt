﻿using Weihnachtsmarkt.DatabaseGeneric;
using Weihnachtsmarkt.Infrastructure.Dtos;
using Weihnachtsmarkt.Infrastructure.Mappers.Interfaces;

namespace Weihnachtsmarkt.Infrastructure.Mappers
{
    public class BookedShiftsDataMapper : IBookedShiftstDataMapper
    {
        public BookedShiftData MapToDto(BookedShift entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new BookedShiftData
            {
                Id = entity.Id,
                Shift = new ShiftData { ShiftId = entity.ShiftId },
                Worker = new WorkerData { Id = entity.WorkerId}
            };
        }

        public BookedShift MapToEntity(BookedShiftData shift)
        {
            if (shift == null)
            {
                return null;
            }

            return new BookedShift
            {
                Id = shift.Id,
                ShiftId = shift.Shift.ShiftId,
                WorkerId = shift.Worker.Id
            };
        }
    }
}