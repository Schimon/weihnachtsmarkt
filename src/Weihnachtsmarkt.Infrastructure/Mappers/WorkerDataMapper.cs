﻿using Weihnachtsmarkt.DatabaseGeneric;
using Weihnachtsmarkt.Infrastructure.Dtos;
using Weihnachtsmarkt.Infrastructure.Mappers.Interfaces;

namespace Weihnachtsmarkt.Infrastructure.Mappers
{
    public class WorkerDataMapper : IWorkerDataMapper
    {
        public WorkerData MapToDto(Worker entity)
        {
            if(entity == null)
            {
                return null;
            }

            return new WorkerData
            {
                Id = entity.Id,
                FirstName = entity.FirstName,
                LastName = entity.LastName
            };
        }

        public Worker MapToEntity(WorkerData worker)
        {
            if (worker == null)
            {
                return null;
            }

            return new Worker
            {
                Id = worker.Id,
                FirstName = worker.FirstName,
                LastName = worker.LastName
            };
        }
    }
}