# Weihnachtsmarkt

Website project for a shift managment system

## Installation

1. **Checkout** project
2. Unzip the **database backup** under **/repo/install/database/weihnachtsmarkt.zip** 
3. **Restore** the database
3. If necessary, adjust the **connection string** in **Web.Config**

---

## Links
|Protokoll	| Link													    |
|-----------|-----------------------------------------------------------|
|SSH		|git@bitbucket.org:Schimon/weihnachtsmarkt.git				|
|HTTPS		|https://Schimon@bitbucket.org/Schimon/weihnachtsmarkt.git  |